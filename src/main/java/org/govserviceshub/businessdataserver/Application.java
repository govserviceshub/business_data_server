package org.govserviceshub.businessdataserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;


/**
 * Created by Vlad-Alexandru.PIRCI on 3/21/2017.
 */
@SpringBootApplication
@ComponentScan
@EntityScan("org.govserviceshub.common.model")
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
